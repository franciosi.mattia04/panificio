-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Apr 15, 2023 alle 18:45
-- Versione del server: 10.4.25-MariaDB
-- Versione PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `panificio`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `cognome` varchar(40) NOT NULL,
  `indirizzo` varchar(40) NOT NULL,
  `ordine` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`id`, `nome`, `cognome`, `indirizzo`, `ordine`) VALUES
(1, 'asdasd', 'asd', 'asd', 'asdads'),
(2, 'mattia', 'ciosi', 'ca mia', 'pane'),
(3, 'mattia', 'franciosi', 'serra', 'pizza'),
(4, 'acas', 'jhjasds', 'saccas', 'scaacs'),
(5, 'ds', 'ccc', 'asdasd', 'asddas'),
(6, 'ds', 'ccc', 'asdasd', 'asddas'),
(7, 'ds', 'ccc', 'asdasd', 'asddas'),
(8, 'ds', 'ccc', 'asdasd', 'asddas'),
(9, 'ds', 'ccc', 'asdasd', 'asddas'),
(10, 'ds', 'ccc', 'asdasd', 'asddas'),
(11, 'ds', 'ccc', 'asdasd', 'asddas'),
(12, 'dvdbdb', 'dvdfvbdv', 'fgbfgbbv', 'dvfvdfgh'),
(13, 'dvdbdb', 'dvdfvbdv', 'fgbfgbbv', 'dvfvdfgh');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
