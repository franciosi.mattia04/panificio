import flask # importa le librerie del framework
from flask import Flask
from flask import request
from flask import render_template
import mysql.connector
con = mysql.connector.connect(user='root', password='',host='localhost', database='panificio')
cursore = con.cursor()
app = Flask(__name__) # crea la web app
@app.route('/') # route di reindirizzamento alla metodo di risposta
def home(): # metodo di risposta (deve restituire una stringa)
    return render_template('index.html')

@app.route('/contatti')
def contatti():
    return render_template('contatti.html')

@app.route('/servizi')
def servizi():
    return render_template('servizi.html')

@app.route('/form')
def load():
    #print(con)

    return render_template('ordina.html')

@app.route('/ordine', methods = ['POST'])
def ordine():
    #val=request.form
    cognome = request.form['cognome']
    nome = request.form['nome']
    indirizzo = request.form['indirizzo']
    ordina = request.form['ordina']
    istruzione="INSERT INTO ordini (nome,cognome,indirizzo,ordine) VALUES (%s,%s,%s,%s)"
    values = (nome,cognome,indirizzo,ordina)
    cursore.execute(istruzione,values)
    con.commit()
    return render_template('ok.html')


@app.route('/noi')
def noi():
    return render_template('chisiamo.html')
        
if __name__ == '__main__':
    app.secret_key = "mysecretkey"
    app.run(
        host="0.0.0.0", # localhost
        port=9000, # porta
    )
    
